# easyforex
![pipeline](https://gitlab.com/damian.ledzinski/easyforex/badges/master/pipeline.svg)

Utilities for Dukascopy's JForex strategies

### Using with Maven project
Add repository section to pom.xml:
```xml
<repositories>
    <repository>
        <id>dynak-nexus</id>
        <name>Dymek - nexus</name>
        <url>https://dymek.utp.edu.pl/nexus/repository/maven-public/</url>
    </repository>
</repositories>
```
Then add dependency:
```xml
<dependency>
    <groupId>easyforex</groupId>
    <artifactId>easyforex</artifactId>
    <!-- find the latest available version here:
        https://dymek.utp.edu.pl/nexus/service/rest/repository/browse/maven-public/easyforex/easyforex/
    -->
    <version>0.7.11</version>
</dependency>
```

### Using with JForex application
- Download latest jar file from: https://dymek.utp.edu.pl/nexus/service/rest/repository/browse/maven-public/easyforex/easyforex/
- Copy it to: HOME/JForex/Strategies/files/
- In Your strategy code use @Library annotation 

### TODO
- strategy builder
- examples
