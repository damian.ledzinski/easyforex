package easyforex.util;

import java.util.List;

/**
 * Utility class for frequently used operations in strategies.
 *
 * @author chazoshtare
 */
public final class EasyOperations {

    /**
     * Detects most recent cross of passed values. Typically these values
     * represent chart lines, where one changes faster than other.
     * <p>
     * Returned integer represents distance of the cross from most recent values
     * (most recent come last). Returns positive value if "fast" line crossed
     * "slow" upwards, negative if downwards and 0 if values did not cross.
     *
     * @param fastValues first valuses list
     * @param slowValues seconf values list
     * @return Positive integer if crossed upwards, negative if downwards, 0 if no cross.
     */
    public static Integer valuesCrossed(List<Double> fastValues, List<Double> slowValues) {
        ValidationUtils.require(!fastValues.isEmpty() && !slowValues.isEmpty(), "Lists can not be empty.");
        ValidationUtils.require(fastValues.size() == slowValues.size(), "Lists have to be equal in size.");

        Integer crossPosition = 0;

        for (int index = fastValues.size() - 1; index > 0; index--) { // from max index to 0
            crossPosition++;

            if (slowValues.get(index - 1) > fastValues.get(index - 1)
                    && slowValues.get(index) < fastValues.get(index)) {
                return crossPosition;
            } else if (slowValues.get(index - 1) < fastValues.get(index - 1)
                    && slowValues.get(index) > fastValues.get(index)) {
                return -crossPosition;
            }
        }
        return 0;
    }
}
