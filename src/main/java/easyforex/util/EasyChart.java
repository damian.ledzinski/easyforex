package easyforex.util;

import com.dukascopy.api.IChart;
import com.dukascopy.api.IChartObject;
import com.dukascopy.api.IContext;
import com.dukascopy.api.IOrder;
import com.dukascopy.api.Instrument;
import com.dukascopy.api.drawings.IOhlcChartObject;
import java.awt.Color;

/**
 * Utility class for charts
 */
public final class EasyChart {

	private final IContext context;
	private final Instrument instrument;
	private final IChart chart;

	public EasyChart(IContext context, Instrument instrument) {
		this.context = context;
		this.instrument = instrument;
		this.chart = context.getChart(instrument);
	}

	/**
	 * Adds indicator to chart
	 *
	 * @param indicatorName indicator name
	 * @param params indicator parameters
	 */
	public void addIndicator(String indicatorName, Object... params) {
		addIndicator(indicatorName, null, params);
	}

	/**
	 * Adds indicator to chart
	 *
	 * @param indicatorName indicator name
	 * @param color indicator color or null if default
	 * @param params indicator params, optional
	 */
	public void addIndicator(String indicatorName, Color color, Object... params) {
		if (chart != null) {
			chart.add(context.getIndicators().getIndicator(indicatorName), params, color != null ? new Color[]{color} : null, null, null);
		}
	}

	/**
	 * Enables showing info on chart
	 */
	public void showIndicatorInfo() {
		if (chart != null) {
			getIOhlcChartObject().setShowIndicatorInfo(true);
		}
	}

	/**
	 * Puts text in given place
	 *
	 * @param time time of putting text
	 * @param price price of putting text
	 * @param text text to put
	 */
	public void putText(long time, double price, String text) {
		putText(time, price, text, null);
	}

	/**
	 * Puts text in given place
	 *
	 * @param time time of putting text
	 * @param price price of putting text
	 * @param text text to put
	 * @param color text color
	 */
	public void putText(long time, double price, String text, Color color) {
		if (chart != null) {
			IChartObject obj = chart.getChartObjectFactory().createText("text" + time, time, price);
			obj.setText(text);
			obj.setOpacity(0.7f);
			obj.setVisibleInWorkspaceTree(false);
			chart.add(obj);
		}
	}

	/**
	 * Puts arrow in given place on the chart
	 *
	 * @param time time of putting arrow
	 * @param price price of putting arrow
	 */
	public void putUpArrow(long time, double price) {
		putUpArrow(time, price, null);
	}

	/**
	 * Puts arrow in given place on the chart
	 *
	 * @param time time of putting arrow
	 * @param price price of putting arrow
	 */
	public void putDownArrow(long time, double price) {
		putDownArrow(time, price, null);
	}

	/**
	 * Puts arrow in given place on the chart
	 *
	 * @param time time of putting arrow
	 * @param price price of putting arrow
	 * @param color arrow color
	 */
	public void putUpArrow(long time, double price, Color color) {
		if (chart != null) {
			IChartObject obj = chart.getChartObjectFactory().createSignalUp("up" + time, time, price);
			obj.setColor(color);
			obj.setOpacity(0.5f);
			obj.setVisibleInWorkspaceTree(false);
			chart.add(obj);
		}
	}

	/**
	 * Puts arrow in given place on the chart
	 *
	 * @param time time of putting arrow
	 * @param price price of putting arrow
	 * @param color arrow color
	 */
	public void putDownArrow(long time, double price, Color color) {
		if (chart != null) {
			IChartObject obj = chart.getChartObjectFactory().createSignalDown("down" + time, time, price);
			obj.setColor(color);
			obj.setOpacity(0.5f);
			obj.setVisibleInWorkspaceTree(false);
			chart.add(obj);
		}
	}

	public void markStopLoss(IOrder order, long time) {
		if (order.isLong()) {
			putUpArrow(time, order.getStopLossPrice(), Color.LIGHT_GRAY);
		} else {
			putDownArrow(time, order.getStopLossPrice(), Color.LIGHT_GRAY);
		}
	}

	public void markTakeProfit(IOrder order, long time) {
		if (order.getTakeProfitPrice() > 0) {
			if (order.isLong()) {
				putDownArrow(time, order.getTakeProfitPrice(), Color.GRAY);
			} else {
				putUpArrow(time, order.getTakeProfitPrice(), Color.GRAY);
			}
		}
	}

	private IOhlcChartObject getIOhlcChartObject() {
		return chart.getAll()
				.stream()
				.filter(obj -> obj instanceof IOhlcChartObject)
				.map(obj -> (IOhlcChartObject) obj)
				.findAny()
				.orElseGet(() -> {
					IOhlcChartObject ohlc = chart.getChartObjectFactory().createOhlcInformer();
					chart.add(ohlc);
					return ohlc;
				});
	}
}
