package easyforex.util;

import com.dukascopy.api.IConsole;
import com.dukascopy.api.IContext;
import com.dukascopy.api.IMessage;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility methods for messages
 *
 * @author damian
 */
public class MessageUtils {

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd HH:mm");

	public static void printMessage(IContext context, IMessage message) {
		IConsole console = context.getConsole();
		if (message.getOrder() != null) {
			String label = message.getOrder().getLabel();
			switch (message.getType()) {
				case ORDER_FILL_OK:
				case ORDER_CHANGED_OK:
					break;
				case ORDER_SUBMIT_OK:
					console.getOut().println(formatMessage(message, "<INFO>", "amount=" + message.getOrder().getAmount()));
					break;
				case ORDER_CLOSE_OK:
				case ORDERS_MERGE_OK:
					if (message.getOrder().getProfitLossInPips() > 0) {
						console.getInfo().println(formatMessage(
								message,
								"<INFO>",
								String.format("%s %s/%s", message.getReasons(), message.getOrder().getProfitLossInPips(), message.getOrder().getProfitLossInAccountCurrency())
						));
					} else {
						console.getWarn().println(formatMessage(
								message,
								"<INFO>",
								String.format("%s %s/%s", message.getReasons(), message.getOrder().getProfitLossInPips(), message.getOrder().getProfitLossInAccountCurrency())
						));
					}
					break;
				case NOTIFICATION:
					console.getNotif().println(formatMessage(message, "<NOTICE>", message.getContent()));
					break;
				case ORDER_CHANGED_REJECTED:
				case ORDER_CLOSE_REJECTED:
				case ORDER_FILL_REJECTED:
				case ORDER_SUBMIT_REJECTED:
				case ORDERS_MERGE_REJECTED:
					console.getErr().println(formatMessage(message, "<WARN>", message.getContent()));
					break;
				default:
					console.getErr().println(formatMessage(message, "<WARN>", message.getContent()));
					break;
			}
		}
	}

	private static String formatMessage(IMessage message, String priority, String content) {
		String label = message.getOrder().getLabel();
		String date = DATE_FORMAT.format(new Date(message.getCreationTime()));

		return String.format("%s %s %s %s %s", label, date, priority, message.getType(), content);
	}
}
