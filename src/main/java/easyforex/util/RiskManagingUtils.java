package easyforex.util;

import com.dukascopy.api.IAccount;
import com.dukascopy.api.IContext;
import com.dukascopy.api.IEngine;
import com.dukascopy.api.ITick;
import com.dukascopy.api.Instrument;
import com.dukascopy.api.JFException;

public final class RiskManagingUtils {

    /**
     * Calculates trade amount using stop loss given in pips and risk percent
     *
     * @param context system context
     * @param instrument instrument
     * @param stopLossPips stop loss in pips
     * @param riskPercent risk in %
     * @return trade amount
     * @throws JFException from Dukascopy low level library
     */
    public static double calculateTradeAmount(IContext context, Instrument instrument, int stopLossPips, double riskPercent) throws JFException {
        IAccount account = context.getAccount();
        double pipPriceOfMillion = context.getUtils().convertPipToCurrency(instrument, account.getAccountCurrency()) * 1000000;
        double baseEquity = account.getBaseEquity();
        double riskEquity = baseEquity * (riskPercent / 100.0);
        // TODO available leverage
        double tradeAmount = riskEquity / (pipPriceOfMillion * stopLossPips);
        return tradeAmount;
    }

    /**
     * Calculates trade amount using stop loss price and risk percent
     *
     * @param context system context
     * @param instrument instrument 
     * @param command order command
     * @param lastTick last tick
     * @param stopLossPrice stop loss price
     * @param riskPercent risk in %
     * @return trade amount
     * @throws JFException from Dukascopy low level library
     */
    public static double calculateTradeAmount(IContext context, Instrument instrument, IEngine.OrderCommand command, ITick lastTick, double stopLossPrice, double riskPercent) throws JFException {
        int stopLossPips = (int) StopLossTakeProfitUtils.getStopLossPips(command, instrument, lastTick, stopLossPrice);
		ValidationUtils.require(stopLossPips > 0, "Stop loss pips should be positive: " + stopLossPips + " @" + lastTick);
        return RiskManagingUtils.calculateTradeAmount(context, instrument, stopLossPips, riskPercent);
    }
}
