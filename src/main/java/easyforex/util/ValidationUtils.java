package easyforex.util;

import easyforex.exception.EasyForexException;

public final class ValidationUtils {

	public static void require(boolean condition, String faultMessage) {
		if (!condition) {
			throw new EasyForexException(faultMessage);
		}
	}
}
