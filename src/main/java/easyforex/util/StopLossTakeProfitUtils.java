package easyforex.util;

import com.dukascopy.api.IEngine;
import com.dukascopy.api.ITick;
import com.dukascopy.api.Instrument;

import java.math.BigDecimal;

/**
 * Stop loss and take profit utilities
 */
public final class StopLossTakeProfitUtils {

	/**
	 * Calculates stop loss price from stop loss in pips
	 *
	 * @param command order command type
	 * @param instrument used financial instrument
	 * @param lastTick tick to calculate on
	 * @param stopLossPips stop loss, if zero then result will be 0
	 * @return stop loss price
	 */
	public static double getStopLossPrice(IEngine.OrderCommand command, Instrument instrument, ITick lastTick, double stopLossPips) {
		if (stopLossPips <= 0) {
			return 0;
		}
		if (command.isLong()) {
			double price = lastTick.getBid();
			return price - stopLossPips * instrument.getPipValue();
		} else {
			double price = lastTick.getAsk();
			return price + stopLossPips * instrument.getPipValue();
		}
	}

	/**
	 * Calculates stop loss in pips from stop loss price
	 *
	 * @param command order command type
	 * @param instrument used financial instrument
	 * @param lastTick tick to calculate on
	 * @param stopLossPrice stop loss, if zero then result will be 0
	 * @return stop loss in pips
	 */
	public static double getStopLossPips(IEngine.OrderCommand command, Instrument instrument, ITick lastTick, double stopLossPrice) {
		if (stopLossPrice <= 0) {
			return 0;
		}
		if (command.isLong()) {
			double price = lastTick.getBid();
			return (price - stopLossPrice) / instrument.getPipValue();
		} else {
			double price = lastTick.getAsk();
			return (stopLossPrice - price) / instrument.getPipValue();
		}
	}

	/**
	 * Calculates take profit price from take profit in pips
	 *
	 * @param command order command type
	 * @param instrument used financial instrument
	 * @param lastTick tick to calculate on
	 * @param takeProfitPips take profit, if zero then result will be 0
	 * @return take profit price
	 */
	public static double getTakeProfitPrice(IEngine.OrderCommand command, Instrument instrument, ITick lastTick, double takeProfitPips) {
		if (takeProfitPips <= 0) {
			return 0;
		}
		if (command.isLong()) {
			double price = lastTick.getBid();
			return price + takeProfitPips * instrument.getPipValue();
		} else {
			double price = lastTick.getAsk();
			return price - takeProfitPips * instrument.getPipValue();
		}
	}

	/**
	 * Calculates take profit in pips from take profit price
	 *
	 * @param command order command type
	 * @param instrument used financial instrument
	 * @param lastTick tick to calculate on
	 * @param takeProfitPrice take profit, if zero then result will be 0
	 * @return take profit in pips
	 */
	public static double getTakeProfitPips(IEngine.OrderCommand command, Instrument instrument, ITick lastTick, double takeProfitPrice) {
		if (takeProfitPrice <= 0) {
			return 0;
		}
		if (command.isLong()) {
			double price = lastTick.getBid();
			return (takeProfitPrice - price) / instrument.getPipValue();
		} else {
			double price = lastTick.getAsk();
			return (price - takeProfitPrice) / instrument.getPipValue();
		}
	}

	/**
	 * Rounds value to pip scale + 1 scale
	 *
	 * @param instrument used financial instrument
	 * @param value value
	 * @return scale
	 */
	public static double round(Instrument instrument, double value) {
		int decimalPlaces = instrument.getPipScale() + 1;
		return (new BigDecimal(value)).setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
}
