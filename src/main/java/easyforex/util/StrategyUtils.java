package easyforex.util;

import com.dukascopy.api.IContext;
import com.dukascopy.api.IEngine;
import com.dukascopy.api.IOrder;
import com.dukascopy.api.Instrument;
import com.dukascopy.api.JFException;
import easyforex.exception.IllegalTradeAmountException;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Utility class for strategies
 */
public final class StrategyUtils {

    /**
     * Subscribe to given instruments
     *
     * @param context     strategy context
     * @param instruments instruments to subscribe
     */
    public static void subscribeInstruments(IContext context, Instrument... instruments) {
        subscribeInstruments(context, Arrays.asList(instruments));
    }

    /**
     * Subscribe to given instruments
     *
     * @param context     strategy context
     * @param instruments instruments to subscribe
     */
    public static void subscribeInstruments(IContext context, Collection<Instrument> instruments) {
        Set<Instrument> subscribed = new HashSet<>(context.getSubscribedInstruments());
        subscribed.addAll(instruments);
        context.setSubscribedInstruments(subscribed, true);
    }

    /**
     * Submits order, using market place and slippage=5
     *
     * @param context         system context
     * @param label           order label, must be unique
     * @param instrument      instrument
     * @param command         order command
     * @param amount          trade amount in millions
     * @param stopLossPrice   stop loss price
     * @param takeProfitPrice take profit price
     * @return order object
     * @throws JFException                 from Dukascopy low level library
     * @throws IllegalTradeAmountException when amount is outside of allowed trade boundaries
     */
    public static IOrder submitOrder(IContext context, String label, Instrument instrument, IEngine.OrderCommand command, double amount, double stopLossPrice, double takeProfitPrice) throws JFException {
        if (command != IEngine.OrderCommand.BUY && command != IEngine.OrderCommand.SELL) {
            throw new RuntimeException("Using this method with " + command + " command has no sense");
        }

        BigDecimal minTradableAmount = getAmountInMillions(BigDecimal.valueOf(instrument.getMinTradeAmount()), instrument);
        BigDecimal maxTradableAmount = getAmountInMillions(BigDecimal.valueOf(instrument.getMaxTradeAmount()), instrument);
        if (minTradableAmount.compareTo(BigDecimal.valueOf(amount)) > 0) {
            throw new IllegalTradeAmountException(String.format("Order amount: %.7f can't be less than minimum allowed: %.6f", amount, minTradableAmount.doubleValue()));
        } else if (maxTradableAmount.compareTo(BigDecimal.valueOf(amount)) < 0) {
            throw new IllegalTradeAmountException(String.format("Order amount: %.7f can't be more than maximum allowed: %.6f", amount, maxTradableAmount.doubleValue()));
        }

        return context.getEngine().submitOrder(label, instrument, command, amount, 0, 5, stopLossPrice, takeProfitPrice);
    }

    public static String getUniqueNameSuffix() {
        return UUID.randomUUID().toString().substring(0, 6);
    }

    private static BigDecimal getAmountInMillions(BigDecimal amountInUnits, Instrument instrument) {
        BigDecimal minTradeAmount = BigDecimal.valueOf(instrument.getMinTradeAmount()).stripTrailingZeros();
        int scale = Math.max(minTradeAmount.scale(), 0);
        scale += 6;
        return amountInUnits.divide(BigDecimal.valueOf(1000000L), scale, 5).stripTrailingZeros();
    }
}
