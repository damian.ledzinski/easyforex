package easyforex.util;

import com.dukascopy.api.*;

/**
 * Util class simplifying various operations relevant to creating orders.
 *
 * @author Eryk Lepszy
 */
public class EasyOrders {

    private final IContext context;
    private final Instrument instrument;
    private final EasyIndicators indicators;

    public EasyOrders(IContext context, Instrument instrument, EasyIndicators indicators) {
        this.context = context;
        this.instrument = instrument;
        this.indicators = indicators;
    }

    /**
     * Calculates stop loss price based on ATR. Stop loss pips are a result of latest ATR value multiplied by
     * specified multiplier.
     *
     * @param command       {@link OfferSide} BUY or SELL
     * @param period        timeframe period
     * @param atrPeriod     ATR indicator period
     * @param atrMultiplier multiplier for pips calculation
     * @return ATR based stop loss price
     * @throws JFException from Dukascopy low level library
     */
    public double calculateAtrStopLoss(IEngine.OrderCommand command, Period period, Integer atrPeriod, Integer atrMultiplier) throws JFException {
        double stopLossValue;
        ITick lastTick = context.getHistory().getLastTick(instrument);

        switch (command) {
            case BUY:
                double lastTickBid = lastTick.getBid();
                stopLossValue = lastTickBid - (indicators.atr(period, OfferSide.ASK, atrPeriod) * atrMultiplier);
                return StopLossTakeProfitUtils.round(instrument, stopLossValue);

            case SELL:
                double lastTickAsk = lastTick.getAsk();
                stopLossValue = lastTickAsk + (indicators.atr(period, OfferSide.BID, atrPeriod) * atrMultiplier);
                return StopLossTakeProfitUtils.round(instrument, stopLossValue);

            default:
                throw new IllegalArgumentException("Order command not compatible with this calculation - " + command);
        }
    }

    /**
     * Calculates take profit price based on ATR. Take profit pips are a result of latest ATR value multiplied by
     * specified multiplier.
     *
     * @param command       {@link OfferSide} BUY or SELL
     * @param period        timeframe period
     * @param atrPeriod     ATR indicator period
     * @param atrMultiplier multiplier for pips calculation
     * @return ATR based take profit price
     * @throws JFException from Dukascopy low level library
     */
    public double calculateAtrTakeProfit(IEngine.OrderCommand command, Period period, Integer atrPeriod, Integer atrMultiplier) throws JFException {
        double takeProfitValue;
        ITick lastTick = context.getHistory().getLastTick(instrument);

        switch (command) {
            case BUY:
                double lastTickAsk = lastTick.getAsk();
                takeProfitValue = lastTickAsk + (indicators.atr(period, OfferSide.BID, atrPeriod) * atrMultiplier);
                return StopLossTakeProfitUtils.round(instrument, takeProfitValue);

            case SELL:
                double lastTickBid = lastTick.getBid();
                takeProfitValue = lastTickBid - (indicators.atr(period, OfferSide.ASK, atrPeriod) * atrMultiplier);
                return StopLossTakeProfitUtils.round(instrument, takeProfitValue);

            default:
                throw new IllegalArgumentException("Order command not compatible with this calculation - " + command);
        }
    }
}
