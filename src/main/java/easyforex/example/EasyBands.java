package easyforex.example;

import com.dukascopy.api.Configurable;
import com.dukascopy.api.IBar;
import com.dukascopy.api.IContext;
import com.dukascopy.api.IEngine;
import com.dukascopy.api.ITick;
import com.dukascopy.api.Instrument;
import com.dukascopy.api.JFException;
import com.dukascopy.api.Library;
import com.dukascopy.api.Period;
import easyforex.base.RiskManagingAbstractStrategy;
import easyforex.exception.EasyForexException;
import easyforex.util.EasyIndicators;

/**
 * Uses BBands
 *
 */
@Library("easyforex-0.7.11.jar")
public class EasyBands extends RiskManagingAbstractStrategy {

	@Configurable("Selected period")
	public Period selectedPeriod = Period.ONE_HOUR;
	@Configurable(value = "BBands time period")
	public int bbandsTimePeriod = 20;
	@Configurable(value = "BBands nb dev up")
	public double bbandsNbDevUp = 2;
	@Configurable(value = "BBands nb dev down")
	public double bbandsNbDevDn = 2;

	public EasyBands() {
		// maximum stop loss
		stopLossPips = 25;
		takeProfitPips = 50;
	}

	@Override
	public void onStart(IContext context) throws JFException {
		super.onStart(context);

		chart.addIndicator("BBANDS", bbandsTimePeriod, bbandsNbDevUp, bbandsNbDevDn, 1);
	}

	@Override
	public void onTick(Instrument instrument, ITick tick) throws JFException {
		trailingStop(tick);
	}

	@Override
	public void onBar(Instrument instrument, Period period, IBar askBar, IBar bidBar) throws JFException {
		if (!instrument.equals(selectedInstrument) || !period.equals(selectedPeriod)) {
			return;
		}

		// if there is no open order
		if (getOrders().isEmpty()) {
			// try to buy
			trySubmitOrder(instrument, period, askBar, bidBar);
		}

	}

	private void trySubmitOrder(Instrument instrument, Period period, IBar askBar, IBar bidBar) throws JFException {

		EasyIndicators.BBandResult bbadns = indicators.bbands(period, bbandsTimePeriod, bbandsNbDevUp, bbandsNbDevDn);

		// checking for Buy
		if (askBar.getClose() > bbadns.getUpperBand()) {

			try {
				submitOrderCalculatingRiskForPips(IEngine.OrderCommand.BUY, stopLossPips, takeProfitPips);
			} catch (EasyForexException e) {
				println(e.toString());
			}
		}

		// checking for Sale
		if (bidBar.getClose() < bbadns.getLowerBand()) {

			try {
				submitOrderCalculatingRiskForPips(IEngine.OrderCommand.SELL, stopLossPips, takeProfitPips);
			} catch (EasyForexException e) {
				println(e.toString());
			}
		}
	}

}
