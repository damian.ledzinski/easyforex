package easyforex.example;

import com.dukascopy.api.Configurable;
import com.dukascopy.api.IBar;
import com.dukascopy.api.IContext;
import com.dukascopy.api.IEngine;
import com.dukascopy.api.Instrument;
import com.dukascopy.api.JFException;
import com.dukascopy.api.Library;
import com.dukascopy.api.Period;
import easyforex.base.RiskManagingAbstractStrategy;
import easyforex.exception.EasyForexException;
import easyforex.util.SarUtils;
import java.awt.Color;

/**
 * Uses two SARs and MACD
 *
 */
@Library("easyforex-0.7.11.jar")
public class EasySar extends RiskManagingAbstractStrategy {

    @Configurable("Selected period")
    public Period selectedPeriod = Period.ONE_HOUR;
    @Configurable(value = "Fast SAR acceleration", stepSize = 0.005)
    public double fastSarAcceleration = 0.02;
    @Configurable(value = "Slow SAR acceleration", stepSize = 0.005)
    public double slowSarAcceleration = 0.01;
    @Configurable(value = "SAR maximum", stepSize = 0.005)
    public double sarMaximum = 0.2;
    @Configurable(value = "MACD fast period")
    public int macdFastPeriod = 12;
    @Configurable(value = "MACD slow period")
    public int macdSlowPeriod = 26;
    @Configurable(value = "MACD signal period")
    public int macdSignalPeriod = 9;
    @Configurable(value = "SAR direction tolerance")
    public int sarDirectionTolerance = 4;

    public EasySar() {
        // maximum stop loss
        stopLossPips = 50;
        takeProfitPips = 0;
    }

    @Override
    public void onStart(IContext context) throws JFException {
        super.onStart(context);

        chart.addIndicator("MACD", macdFastPeriod, macdSlowPeriod, macdSignalPeriod);
        chart.addIndicator("SAR", fastSarAcceleration, sarMaximum);
        chart.addIndicator("SAR", Color.ORANGE, slowSarAcceleration, sarMaximum);

    }


    @Override
    public void onBar(Instrument instrument, Period period, IBar askBar, IBar bidBar) throws JFException {

        if (!instrument.equals(selectedInstrument) || !period.equals(selectedPeriod)) {
            return;
        }

        // moving stop loss
        sarBasedTrailingStop(period, askBar, bidBar, slowSarAcceleration, sarMaximum);

        // if there is no open order
        if (getOrders().isEmpty()) {
            // try to buy
            trySubmitOrder(instrument, period, askBar, bidBar);
        }
    }

    private void trySubmitOrder(Instrument instrument, Period period, IBar askBar, IBar bidBar) throws JFException {

        double macdHist = indicators.macd(period, macdFastPeriod, macdSignalPeriod, macdSignalPeriod).getMacdHist();
        final int maxInRow = sarDirectionTolerance + 1;

        // checking for Buy
        if (macdHist > 0) {
            int fastSarNum = SarUtils.lastRisingSarsNumber(indicators, period, fastSarAcceleration, sarMaximum, maxInRow + 1);
            int slowSarNum = SarUtils.lastRisingSarsNumber(indicators, period, slowSarAcceleration, sarMaximum, maxInRow + 1);

            if (fastSarNum > 0 && slowSarNum > 0 && fastSarNum <= maxInRow && slowSarNum <= maxInRow && fastSarNum <= slowSarNum) {
                double stopLoss = SarUtils.getSarBasedStopLoss(context, true, instrument, period, bidBar, askBar, slowSarAcceleration, sarMaximum);

                try {
                    submitOrderCalculatingRisk(IEngine.OrderCommand.BUY, stopLoss, 0);
                } catch (EasyForexException e) {
                    println(e.toString());
                }
            }
        }

        // checking for Sale
        if (macdHist < 0) {
            int fastSarNum = SarUtils.lastFallingSarsNumber(indicators, period, fastSarAcceleration, sarMaximum, maxInRow + 1);
            int slowSarNum = SarUtils.lastFallingSarsNumber(indicators, period, slowSarAcceleration, sarMaximum, maxInRow + 1);

            if (fastSarNum > 0 && slowSarNum > 0 && fastSarNum <= maxInRow && slowSarNum <= maxInRow && fastSarNum <= slowSarNum) {
                double stopLoss = SarUtils.getSarBasedStopLoss(context, false, instrument, period, bidBar, askBar, slowSarAcceleration, sarMaximum);

                try {
                    submitOrderCalculatingRisk(IEngine.OrderCommand.SELL, stopLoss, 0);
                } catch (EasyForexException e) {
                    println(e.toString());
                }
            }
        }
    }

}
