package easyforex.exception;

/**
 * @author Eryk Lepszy
 */
public class IllegalTradeAmountException extends RuntimeException {

    public IllegalTradeAmountException(String message) {
        super(message);
    }

    public IllegalTradeAmountException(Throwable cause) {
        super(cause);
    }

    public IllegalTradeAmountException(String message, Throwable cause) {
        super(message, cause);
    }
}
