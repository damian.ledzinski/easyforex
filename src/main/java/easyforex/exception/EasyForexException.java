package easyforex.exception;

/**
 * Exception for easyforex methods
 * @author damian
 */
public class EasyForexException extends RuntimeException {

	public EasyForexException(String message) {
		super(message);
	}

}
