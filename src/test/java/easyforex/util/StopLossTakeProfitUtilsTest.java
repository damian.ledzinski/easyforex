package easyforex.util;

import com.dukascopy.api.IEngine;
import com.dukascopy.api.ITick;
import com.dukascopy.api.Instrument;
import com.dukascopy.charts.data.datacache.TickData;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author Eryk Lepszy
 */
public class StopLossTakeProfitUtilsTest {

    @Test
    public void getStopLossPrice_BuyCommand_ReturnsCorrectPrice() {
        ITick tick = new TickData(1L, 1.01200, 1.00800, 1.0, 1.0);
        double actual = StopLossTakeProfitUtils.getStopLossPrice(IEngine.OrderCommand.BUY, Instrument.EURPLN, tick, 30);
        double expected = 1.00500;

        assertEquals(expected, actual, 0.000001);
    }


    @Test
    public void getStopLossPrice_SellCommand_ReturnsCorrectPrice() {
        ITick tick = new TickData(1L, 1.01200, 1.00800, 1.0, 1.0);
        double actual = StopLossTakeProfitUtils.getStopLossPrice(IEngine.OrderCommand.SELL, Instrument.EURPLN, tick, 30);
        double expected = 1.01500;

        assertEquals(expected, actual, 0.000001);
    }

    @Test
    public void getStopLossPips_BuyCommand_ReturnsCorrectValue() {
        ITick tick = new TickData(1L, 1.01200, 1.00800, 1.0, 1.0);
        double actual = StopLossTakeProfitUtils.getStopLossPips(IEngine.OrderCommand.BUY, Instrument.EURPLN, tick, 1.00500);
        double expected = 30.0;

        assertEquals(expected, actual, 0.001);
    }

    @Test
    public void getStopLossPips_SellCommand_ReturnsCorrectValue() {
        ITick tick = new TickData(1L, 1.01200, 1.00800, 1.0, 1.0);
        double actual = StopLossTakeProfitUtils.getStopLossPips(IEngine.OrderCommand.SELL, Instrument.EURPLN, tick, 1.01500);
        double expected = 30.0;

        assertEquals(expected, actual, 0.001);
    }

    @Test
    public void getTakeProfitPrice_BuyCommand_ReturnsCorrectPrice() {
        ITick tick = new TickData(1L, 1.01200, 1.00800, 1.0, 1.0);
        double actual = StopLossTakeProfitUtils.getTakeProfitPrice(IEngine.OrderCommand.BUY, Instrument.EURPLN, tick, 50);
        double expected = 1.01300;

        assertEquals(expected, actual, 0.000001);
    }

    @Test
    public void getTakeProfitPrice_SellCommand_ReturnsCorrectPrice() {
        ITick tick = new TickData(1L, 1.01200, 1.00800, 1.0, 1.0);
        double actual = StopLossTakeProfitUtils.getTakeProfitPrice(IEngine.OrderCommand.SELL, Instrument.EURPLN, tick, 50);
        double expected = 1.00700;

        assertEquals(expected, actual, 0.000001);
    }

    @Test
    public void getTakeProfitPips_BuyCommand_ReturnsCorrectValue() {
        ITick tick = new TickData(1L, 1.01200, 1.00800, 1.0, 1.0);
        double actual = StopLossTakeProfitUtils.getTakeProfitPips(IEngine.OrderCommand.BUY, Instrument.EURPLN, tick, 1.01300);
        double expected = 50.0;

        assertEquals(expected, actual, 0.001);
    }

    @Test
    public void getTakeProfitPips_SellCommand_ReturnsCorrectValue() {
        ITick tick = new TickData(1L, 1.01200, 1.00800, 1.0, 1.0);
        double actual = StopLossTakeProfitUtils.getTakeProfitPips(IEngine.OrderCommand.SELL, Instrument.EURPLN, tick, 1.00700);
        double expected = 50.0;

        assertEquals(expected, actual, 0.001);
    }
}
